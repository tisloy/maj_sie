
--
-- sur la bd stagiaire
--

CREATE TABLE `ind_bilan` (
  `quantite_bilan` decimal(15,3) DEFAULT NULL,
  `totalP` decimal(15,3) DEFAULT NULL,
  `taux_conversion` decimal(15,3) DEFAULT NULL,
  `Composant_codeCompo` varchar(45) NOT NULL,
  `Pays_codePays` varchar(45) NOT NULL,
  `Produit_codeProduit` varchar(45) NOT NULL,
  `Temps_annee` int(11) NOT NULL,
  `codeMatierePremiere` varchar(255) DEFAULT NULL,
  `confirme` tinyint(1) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


DELIMITER $$
CREATE TRIGGER `Add_On_Ind_Bilan` AFTER INSERT ON `ind_bilan` FOR EACH ROW BEGIN
insert into edw.fait_ind_bilan values (NEW.quantite_bilan,NEW.totalP,NEW.taux_conversion,NEW.Composant_codeCompo,NEW.Pays_codePays,NEW.Produit_codeProduit,NEW.Temps_annee,NEW.codeMatierePremiere,NEW.confirme);
end
$$
DELIMITER ;


CREATE TABLE `ind_bilan_energetique` (
  `quantite_bilan` decimal(15,3) DEFAULT NULL,
  `totalP` decimal(15,3) DEFAULT NULL,
  `taux_conversion` decimal(15,3) DEFAULT NULL,
  `Composant_codeCompo` varchar(45) NOT NULL,
  `Pays_codePays` varchar(45) NOT NULL,
  `Produit_codeProduit` varchar(45) NOT NULL,
  `Temps_annee` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

DELIMITER $$
CREATE TRIGGER `Add_On_Ind_Bilan_Energetique` AFTER INSERT ON `ind_bilan_energetique` FOR EACH ROW BEGIN
insert into edw.fait_ind_bilan_energetique values (NEW.quantite_bilan,NEW.totalP,NEW.taux_conversion,NEW.Composant_codeCompo,NEW.Pays_codePays,NEW.Produit_codeProduit,NEW.Temps_annee);
end
$$
DELIMITER ;

CREATE TABLE `facteur_em` (
  `valeur` decimal(15,3) DEFAULT NULL,
  `composant` varchar(15) NOT NULL,
  `Pays_codePays` varchar(45) NOT NULL,
  `Produit_codeProduit` varchar(45) NOT NULL,
  `Temps_annee` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

DELIMITER $$
CREATE TRIGGER `Add_On_Facteur_Em` AFTER INSERT ON `facteur_em` FOR EACH ROW BEGIN
insert into edw.fait_facteur_em values (NEW.valeur,NEW.composant,NEW.Pays_codePays,NEW.Produit_codeProduit,NEW.Temps_annee);
end
$$
DELIMITER ;

CREATE TABLE `ind_bilan_em` (
  `valeur` decimal(15,3) DEFAULT NULL,
  `composant` varchar(15) NOT NULL,
  `Pays_codePays` varchar(45) NOT NULL,
  `Produit_codeProduit` varchar(45) NOT NULL,
  `Temps_annee` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='in';

DELIMITER $$
CREATE TRIGGER `Add_On_Ind_Bilan_Em` AFTER INSERT ON `ind_bilan_em` FOR EACH ROW BEGIN
insert into edw.fait_ind_bilan_em values (NEW.valeur,NEW.composant,NEW.Pays_codePays,NEW.Produit_codeProduit,NEW.Temps_annee);
end
$$
DELIMITER ;
