#!/bin/bash
echo " "
echo " "
echo " "

heure=$(date +%H':'%M)
jour=$(date +%d'/'%m'/'%Y)

echo "------------------------------------------------"
echo "|  BIENVENUE - DEPLOIEMENT DU PROJET SIE-UEMOA |"
echo "|              $jour  - $heure                 |"
echo "|               BY AKASI GROUP                 |"
echo "------------------------------------------------"

echo " "
echo " "

parent=$pwd


sleep 1
echo " "

echo " "

cd /tmp

if [ -d "/tmp/MAJ_SIE" ]; then
    rm -rf MAJ_SIE
    mkdir MAJ_SIE
else
    mkdir MAJ_SIE
fi

echo " "
echo "Récupération des tables de la base de données SIE-UEMOA"
echo " "

cd MAJ_SIE

git config --global http.postBuffer 1000000000
git clone https://tisloy@bitbucket.org/tisloy/maj_sie.git

sleep 1

echo " "
echo " "

cd maj_sie

if [ -z "$(ls -A /tmp/MAJ_SIE/maj_sie)" ]; then
    echo "Echec d'authentification"
	echo " "
	echo " "

else
    echo "Clonage des tables effectué avec succès !"


	echo "-----------------------------------------------------"
	echo "3. === Configuration de la base de données locale ==="
	echo "-----------------------------------------------------"
	
	chmod +x maj.sh
	chmod 0755 maj.sh
	./maj.sh

	$(mysql -u root -p -e "use sie_uemoa_local;source sqllocal.sql ;")

	if [ $?==1 ];then
		echo "Base de données locale configurée avec succès !"
	else
		echo "Mot de passe incorrect, veuillez réessayer !"
		echo " "
		echo " "
		
		$(mysql -u root -p -e "use sie_uemoa_local;source sqllocal.sql ;")
		
		if [ $?==1 ];then
			echo "Base de données locale configurée avec succès !"
		else
			echo "Mot de passe incorrect, veuillez réessayer !"
			echo " "
			echo " "
		
			$(mysql -u root -p -e "use sie_uemoa_local;source sqllocal.sql ;")
			
			if [ $?==1 ];then
				echo "Base de données locale configurée avec succès !"
			else
				echo "Echec de configuration de la BD locale !"
			fi
		fi
	fi


	echo " "
	echo " "
	sleep 1


	echo "--------------------------------------------------------"
	echo "4. === Configuration de la base de données stagiaire ==="
	echo "--------------------------------------------------------"
	echo " "
	echo " "


	$(mysql -u root -p -e "use sie_uemoa_esb;source sqlstagiaire.sql;")

	if [ $?==1 ];then
		echo "Base de données stagiaire configurée avec succès !"
	else
		echo "Mot de passe incorrect, veuillez réessayer !"
		echo " "
		echo " "
		
		$(mysql -u root -p -e "use sie_uemoa_esb;source sqlstagiaire.sql;")
		
		if [ $?==1 ];then
			echo "Base de données stagiaire configurée avec succès !"
		else
			echo "Mot de passe incorrect, veuillez réessayer !"
			echo " "
			echo " "
		
			$(mysql -u root -p -e "use sie_uemoa_esb;source sqlstagiaire.sql;")
			
			if [ $?==1 ];then
				echo "Base de données stagiaire configurée avec succès !"
			else
				echo "Echec de configuration de la BD stagiaire !"
			fi
		fi
	fi

	echo " "
	echo " "
	sleep 1


	echo "-------------------------------------------------"
	echo "5. === Configuration de l'entrepôt de données ==="
	echo "-------------------------------------------------"
	
	echo " "
	echo " "

	$(mysql -u root -p -e "use edw;source sqledw.sql;")

	if [ $?==1 ];then
		echo "Entrepôt de données configurée avec succès !"
	else
		echo "Mot de passe incorrect, veuillez réessayer !"
		echo " "
		echo " "
		
		$(mysql -u root -p -e "use edw;source sqledw.sql;")
	
		if [ $?==1 ];then
			echo "Entrepôt de données configurée avec succès !"
		else
			echo "Mot de passe incorrect, veuillez réessayer !"
			echo " "
			echo " "
		
			$(mysql -u root -p -e "use edw;source sqledw.sql;")
			
			if [ $?==1 ];then
				echo "Entrepôt de données configurée avec succès !"
			else
				echo "Echec de configuration de l'entrepôt de données !"
			fi
		fi
	fi

	echo " "
	echo " "
	sleep 1


fi

cd $parent
