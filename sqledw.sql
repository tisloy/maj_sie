
--
-- sur edw
--

CREATE TABLE `fait_facteur_em` (
  `valeur` decimal(15,3) DEFAULT NULL,
  `composant` varchar(15) NOT NULL,
  `Pays_codePays` varchar(45) NOT NULL,
  `Produit_codeProduit` varchar(45) NOT NULL,
  `Temps_annee` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE `fait_ind_bilan_em` (
  `valeur` decimal(15,3) DEFAULT NULL,
  `composant` varchar(15) NOT NULL,
  `Pays_codePays` varchar(45) NOT NULL,
  `Produit_codeProduit` varchar(45) NOT NULL,
  `Temps_annee` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='in';

CREATE TABLE `fait_ind_bilan` (
  `quantite_bilan` decimal(15,3) DEFAULT NULL,
  `totalP` decimal(15,3) DEFAULT NULL,
  `taux_conversion` decimal(15,3) DEFAULT NULL,
  `Composant_codeCompo` varchar(45) NOT NULL,
  `Pays_codePays` varchar(45) NOT NULL,
  `Produit_codeProduit` varchar(45) NOT NULL,
  `Temps_annee` int(11) NOT NULL,
  `codeMatierePremiere` varchar(255) DEFAULT NULL,
  `confirme` tinyint(1) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


CREATE TABLE `fait_ind_bilan_energetique` (
  `quantite_bilan` decimal(15,3) DEFAULT NULL,
  `totalP` decimal(15,3) DEFAULT NULL,
  `taux_conversion` decimal(15,3) DEFAULT NULL,
  `Composant_codeCompo` varchar(45) NOT NULL,
  `Pays_codePays` varchar(45) NOT NULL,
  `Produit_codeProduit` varchar(45) NOT NULL,
  `Temps_annee` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
