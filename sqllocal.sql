--
-- sur la bd locale
--
DELIMITER $$
CREATE TRIGGER `Add_On_Ind_Bilan` AFTER INSERT ON `ind_bilan` FOR EACH ROW BEGIN
insert into sie_uemoa_esb.ind_bilan values (NEW.quantite_bilan,NEW.totalP,NEW.taux_conversion,NEW.Composant_codeCompo,NEW.Pays_codePays,NEW.Produit_codeProduit,NEW.Temps_annee,NEW.codeMatierePremiere,NEW.confirme);
end
$$
DELIMITER ;

DELIMITER $$
CREATE TRIGGER `Add_On_Ind_Bilan_Energetique` AFTER INSERT ON `ind_bilan_energetique` FOR EACH ROW BEGIN
insert into sie_uemoa_esb.ind_bilan_energetique values (NEW.quantite_bilan,NEW.totalP,NEW.taux_conversion,NEW.Composant_codeCompo,NEW.Pays_codePays,NEW.Produit_codeProduit,NEW.Temps_annee);
end
$$
DELIMITER ;
